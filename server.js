const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {
  // we can access HTTP headers
  console.log("request made");

  try {
    let urlData = req.url.split("/");
    switch (urlData[1]) {
      case "html":
        {
          let data = fs.readFileSync("get.html");
            if (data) {
            res.setHeader("Content-type", "text/html");
            res.end(data);
          }
        }
        break;
      case "json":
        {
          let data = fs.readFileSync("get.json");
          if (data) {
            res.setHeader("Content-type", "application/json");
            res.end(data);
          }
        }
        break;
      case "uuid":
        {
          let data = { uuid: uuidv4() };

          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(JSON.stringify(data));
        }
        break;
      case "status":
        {
          if (urlData.length === 3) {
            let statusInfo = http.STATUS_CODES;
            if (statusInfo.hasOwnProperty(urlData[2])) {
              res.writeHead(urlData[2], { "Content-Type": "application/json" });
              res.end(`${urlData[2]} ${statusInfo[urlData[2]]}`);
            } else {
              throw new Error("status not exist status is wrong");
            }
          } else {
            res.end("wrong url");
          }
        }
        break;
      case "delay":
        {
          if (urlData.length === 3) {
            setInterval(() => {
              res.end(`200 ok`);
            }, 1000 * urlData[2]);
          } else {
            throw new Error("wrong url for delay");
          }
        }
        break;
      default: {
        throw new Error (`something is wrong does not contain any case `);
      }
    }
  } catch (error) {
    res.writeHead(500, { "Content-Type": "text/plain" });
    res.end(JSON.stringify(error.message));
  }
});

server.listen(8000, "localhost", () => {
  console.log("listning");
});
